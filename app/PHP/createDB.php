<?php
/*
DESC => This scripts builds DB from the ground up (with dropping the old DB)
=================================
URL => twojkandydat.comxa.com/PHP/createDB.php
=================================
Query parameters => NONE
=================================
JSON as input => NONE
=================================
Response => 
	HTTP 200 => Success
	HTTP 500 => SQL error
*/
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

	$conn = new mysqli("eryzim.nazwa.pl:3307", "eryzim", "Andrzejryl2015", "eryzim");
	// Drop tables
	$drop_result = $conn->query("DROP TABLE IF EXISTS
		Comments, CandidatesToEvents, Tweets, Candidates, Debates, Votings, ElectionRallies");

	checkResult($drop_result, "DROP");
	
	// Create Candidates table
	$create_candidates_result = $conn->query("
		CREATE TABLE Candidates (
			id INT NOT NULL AUTO_INCREMENT,
			name VARCHAR(30) NOT NULL,
			surname VARCHAR(30) NOT NULL,
			party VARCHAR(30) NOT NULL,
			age INT(2) NOT NULL,
			description VARCHAR(500) NOT NULL,
			image VARCHAR(200) NOT NULL,
			twitterID VARCHAR(50) NOT NULL,
			color VARCHAR(7) NOT NULL,
			PRIMARY KEY(id)
		)");

	checkResult($create_candidates_result, "Candidates");
	
	// Create Debates table
	$create_debates_result = $conn->query("
		CREATE TABLE Debates (
			id INT NOT NULL,
			TVname VARCHAR(30) NOT NULL,
			description VARCHAR(300) NOT NULL,
			host VARCHAR(40) NOT NULL,
			PRIMARY KEY(id)
		)");

	checkResult($create_debates_result, "Debates");

	// Create Votings table
	$create_votings_result = $conn->query("
		CREATE TABLE Votings (
			id INT NOT NULL,
			description VARCHAR(300) NOT NULL,
			PRIMARY KEY(id)
		)");

	checkResult($create_votings_result, "Votings");

	// Create ElectionRallies table
	$create_election_rallies_result = $conn->query("
		CREATE TABLE ElectionRallies (
			id INT NOT NULL,
			description VARCHAR(300) NOT NULL,
			PRIMARY KEY(id)
		)");

	checkResult($create_election_rallies_result, "ElectionRallies");

	// Create Tweets table
	$create_tweets_result = $conn->query("
		CREATE TABLE Tweets (
			id INT NOT NULL AUTO_INCREMENT,
			content VARCHAR(300) NOT NULL,
			candidate_id INT NOT NULL,
			time DATETIME,
			user_login VARCHAR(30),
			user_name VARCHAR (30),
			user_pic VARCHAR(300),
			PRIMARY KEY(id),
			FOREIGN KEY(candidate_id) REFERENCES Candidates(id)
		)");

	checkResult($create_tweets_result, "Tweets");

	// Create CandidatesToEvents table
	$create_candidates_to_events_result = $conn->query("
		CREATE TABLE CandidatesToEvents (
			id INT NOT NULL AUTO_INCREMENT,
			event_id INT NOT NULL,
			candidate_id INT NOT NULL,
			location VARCHAR(80) NOT NULL,
			time DATETIME NOT NULL,
			PRIMARY KEY(id),
			FOREIGN KEY(candidate_id) REFERENCES Candidates(id)
		)");

	checkResult($create_candidates_to_events_result, "CandidatesToEvents");
	
	// Create Comments table
	$create_comments_result = $conn->query("
		CREATE TABLE Comments (
			id INT NOT NULL AUTO_INCREMENT,
			event_id INT NOT NULL,
			nickname VARCHAR(30) NOT NULL,
			content VARCHAR(30) NOT NULL,
			date DATETIME NOT NULL,
			user_id VARCHAR(60) NOT NULL,
			PRIMARY KEY(id),
			FOREIGN KEY(event_id) REFERENCES CandidatesToEvents(id)
		)");

	checkResult($create_comments_result, "Comments");
	
	$conn->close();

	header("HTTP/1.1 200 OK");
	echo 'Success';
		

function checkResult($result, $name) {
	if (!$result) {
		header("HTTP/1.1 500 Internal Server Error");
		echo "Failure in: ".$name;
		echo mysqli_error($conn);
	}
}

?>
