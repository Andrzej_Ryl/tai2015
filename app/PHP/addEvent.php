<?php
/*
DESC => Adds an event
=================================
URL => twojkandydat.comxa.com/PHP/addEvent.php
=================================
Query parameters => 
	type (== DEBATE || VOTINGS || RALLIES || TWEET): Add an event of that type
=================================
JSON as input => 
		if (type == DEBATE)
			{tvname: string,
			description: string,
			host: string,
			candidate_id: int,
			location: string,
			time: datetime}
		else if (type == VOTINGS || RALLIES)
			{description: string,
			candidate_id: int,
			location: string,
			time: datetime}
		else if (type == TWEET)
			{content: string,
			candidate_id: int,
			time: datetime,
			user_login: string,
			user_name: string,
			user_pic: string}
			
=================================
Response => 
	HTTP 200 => Success
	HTTP 500 => SQL error
	HTTP 400 => Supply type! || Wrong type!
*/

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");


	$conn = new mysqli("eryzim.nazwa.pl:3307", "eryzim", "Andrzejryl2015", "eryzim");
	// Check the type of an event
	$type = $_GET['type'];

	if (empty($type)) {
		header("HTTP/1.1 400 Bad request");
		$conn->close();
		die('Supply type!');
	}
	/*
	* Collect all Details from Angular HTTP Request.
	*/ 
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	
	// Build query
	if ($type == 'DEBATE') {
		$tvname = $request->tvname;
		$description = $request->description;
		$host = $request->host;
		$id = calculateID($conn, 'Debates');
		
		addCandidateToEvent($conn, $id, $request);
		
		$query = "INSERT INTO Debates (id,
				TVname, description, host)
				VALUES (".$id.",'".$tvname."','".$description."','".$host."')";
	} else if ($type == 'VOTINGS') {
		$description = $request->description;
		$id = calculateID($conn, 'Votings');
		
		addCandidateToEvent($conn, $id, $request);
	
		$query = "INSERT INTO Votings (id,
				description)
				VALUES (".$id.",'".$description."')";
	} else if ($type == 'RALLIES') {
		$description = $request->description;
		$id = calculateID($conn, 'ElectionRallies');
		
		addCandidateToEvent($conn, $id, $request);
		
		$query = "INSERT INTO ElectionRallies (id,
				description)
				VALUES (".$id.",'".$description."')";
	} else if ($type == 'TWEET') {
		$content = $request->content;
		$candidate_id = $request->candidate_id;
		$time = $request->time;
		$user_login = $request->user_login;
		$user_name = $request->user_name;
		$user_pic = $request->user_pic;
		$id = calculateID($conn, 'Tweets');
		
		$query = "INSERT INTO Tweets (
				id, content, candidate_id, time, user_login, user_name, user_pic)
				VALUES (".$id.",'".$content."',".$candidate_id.",'".$time."', '".$user_login."', '".$user_name."', '".$user_pic."')";
		
	} else {
		header("HTTP/1.1 400 Bad request");
		$conn->close();
		die('Wrong type!');
	}

	// Insert a record to the table
	$insert_result = $conn->query($query);
	
	$conn->close();

	if (!$conn || !$insert_result) {
		header("HTTP/1.1 500 Internal Server Error");
		echo mysqli_error($conn);
	} else {
		header("HTTP/1.1 200 OK");
		echo "Success";
	}
	
	// This function add connection between event and candidate
	function addCandidateToEvent($conn, $id, $request) {
		$candidate_id = $request->candidate_id;
		$location = $request->location;
		$time = $request->time;
		
		$query = "INSERT INTO CandidatesToEvents (
				event_id, candidate_id, location, time)
				VALUES (".$id.",".$candidate_id.",'".$location."','".$time."')";		
				
		// Insert a record to the table
		$insert_result = $conn->query($query);

		if (!$conn || !$insert_result) {
			header("HTTP/1.1 500 Internal Server Error");
			echo mysqli_error($conn);
		} else {
			header("HTTP/1.1 200 OK");
			echo "Success";
		}
	}
	
	// This method checks how many records do we have in a particular table
	// and calculates id for a new record (we have to have different ids for different event tables)
	function calculateID($conn, $tableName) {
		$result = $conn->query('SELECT COUNT(*) FROM '.$tableName);
		$count = $result->fetch_array(MYSQLI_ASSOC);
		$count = $count["COUNT(*)"];
		
		if ($count == 0) {
			if ($tableName == 'Debates') {
				$id = 1;
			}
			else if ($tableName == 'Votings') {
				$id = 2;
			}
			else if ($tableName == 'ElectionRallies') {
				$id = 3;
			} 
			else if ($tableName == 'Tweets') {
				$id = 4;
			}
		} else {
			$biggestID = $conn->query('SELECT id FROM '.$tableName.' LIMIT 1');
			$id = $biggestID->fetch_array(MYSQLI_ASSOC);
			$id = $id["id"];
			$id = $id + 4;
		}
		
		return $id;
	}

?>
