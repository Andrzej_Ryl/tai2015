<?php
/*
DESC => Returns all comments 
=================================
URL => twojkandydat.comxa.com/PHP/getComments.php
=================================
Query parameters => 
	eventID: Give me comments for this event
=================================
JSON as input => NONE
=================================
Response => 
	HTTP 200 => {records: [{id:int,
							event_id: int,
							nickname: string,
							content: string,
							date: string,
							user_id: string},
							{id:int,
							event_id: int,
							nickname: string,
							content: string,
							date: string,
							user_id: string},
							...]}
	HTTP 500 => SQL error
	HTTP 400 => Supply eventID
*/

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

	// Get parameters from AngularJS
	$event_id = $_GET['eventID']; 

	if (empty($event_id)) {
		header("HTTP/1.1 400 Bad request");
		die('Supply eventID');
	}
	
	// Build query upon parameters given by client
	$query = "SELECT * FROM Comments
				WHERE ";
				
	$query = $query."event_id = ".$event_id;
	
	$conn = new mysqli("eryzim.nazwa.pl:3307", "eryzim", "Andrzejryl2015", "eryzim");
	// Get comments connected to given event
	$result = $conn->query($query);

	if(!$result) {
		header("HTTP/1.1 500 Internal Server Error");
		die(mysqli_error($conn));
	} 
	$outp = "";
	while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
		$date = date_create($rs["date"]);
		$bullshit_date = date_format($date, 'Y-m-d')."T".date_format($date, 'H:i:s')."Z";
		if ($outp != "") {$outp .= ",";}
		$outp .= '{';
		$outp .= '"id":"'  . $rs["id"] . '",';
		$outp .= '"event_id":"'  . $rs["event_id"] . '",';
		$outp .= '"nickname":"'. $rs["nickname"]     . '",';
		$outp .= '"content":"'  . $rs["content"] . '",';
		$outp .= '"date":"'  . $bullshit_date . '",';
		$outp .= '"user_id":"'. $rs["user_id"]     . '"}';
	}
	$outp ='{"records":['.$outp.']}';
	$conn->close();

	header("HTTP/1.1 200 OK");
	echo($outp);

?>
