<?php
/*
DESC => Adds a candidate
=================================
URL => twojkandydat.comxa.com/PHP/addCandidate.php
=================================
Query parameters => NONE
=================================
JSON as input => {name: string,
				surname: string,
				party: string,
				age: int,
				description: string,
				image: string,
				twitterID: string}
=================================
Response => 
	HTTP 200 => Success
	HTTP 500 => SQL error
*/

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

	/*
	* Collect all Details from Angular HTTP Request.
	*/ 
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$name = $request->name;
	$surname = $request->surname;
	$party = $request->party;
	$age = $request->age;
	$description = $request->description;
	$image = $request->image;
	$twitterID = $request->twitterID;

	$conn = new mysqli("eryzim.nazwa.pl:3307", "eryzim", "Andrzejryl2015", "eryzim");
	// Insert a record to the table
	$insert_result = $conn->query("INSERT INTO Candidates (
						name, surname, party, age, description, image, twitterID)
						VALUES ('".$name."','".$surname."','".$party."','".$age."','".$description."','".$image."','".$twitterID."')");


	$conn->close();

	if (!$conn || !$insert_result) {
		header("HTTP/1.1 500 Internal Server Error");
		echo mysqli_error($conn);
	} else {
		header("HTTP/1.1 200 OK");
		echo "Success";
	}

?>
