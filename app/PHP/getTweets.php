<?php
/*
DESC => Returns list of tweets
=================================
URL => twojkandydat.comxa.com/PHP/getTweets.php
=================================
Query parameters => 
	id			: Give me one tweet with that id (id from our DB)
	limit		: Give me last limit tweets (no matter what candidate)
	offset		: Give me offset tweets older than tweet with supplied id
	offset_id	: Id of the tweet from which I need to start offsetting
	candidate_id: Give me tweets connected to this candidate
=================================
JSON as input => NONE
=================================
Response => 
	HTTP 200 => {records: [{id: int, 
							content: string,
							candidate_id: int,
							time: datetime,
							user_login: string,
							user_name: string;
							user_pic: string,
							type: string (= TWEET)},
							...
							]}
	HTTP 400 => Supply at least one of parameters! I won't return all of freaking tweets!
	HTTP 404 => There is no tweet with that ID. Maybe you wanted an event?
*/

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

	$id = $_GET['id'];
	$offset = $_GET['offset'];
	$offset_id = $_GET['offset_id'];
	$candidate_id = $_GET['candidate_id'];
	$limit = $_GET['limit'];
	$conn = new mysqli("eryzim.nazwa.pl:3307", "eryzim", "Andrzejryl2015", "eryzim");
	
	// Build query
	$query = "SELECT id, content, candidate_id, time, user_login, user_name, user_pic FROM Tweets ";
	
	if (!empty($id)) {
		// We just need one tweet 
		$query = $query . "WHERE id = " . $id;
	}
	else if (!empty($candidate_id)) {
		// Filter out tweets by candidate id
		$query = $query . "WHERE candidate_id = " . $candidate_id;
	}	
	else if (empty($id) && empty($limit) && empty($offset) && empty($candidate_id)) {
		header("HTTP/1.1 400 Bad Request");
		die("Supply at least one of parameters! I won't return all of freaking tweets!");
	}
	
	// Add ordering and limit
	$query = $query . " ORDER BY time desc ";
	
	if (!empty($limit))
		$query = $query . " LIMIT " . $limit;
	
	if (!empty($id)) {
		// Check if tweet with that id exists
		$result = $conn->query("SELECT COUNT(*) FROM Tweets
								WHERE id = ".$id);
		$exist = $result->fetch_array(MYSQLI_ASSOC);
		$exist = ($exist["COUNT(*)"] == 1);
		
		if (!$exist) {
			header("HTTP/1.1 404 Resource not found");
			die("There is no tweet with that ID. Maybe you wanted an event?");
		}
		
	} 
	
	$result = $conn->query($query);
	
	if(!$result) {
		header("HTTP/1.1 500 Internal Server Error");
		die(mysqli_error($conn));
	}

	if (!empty($offset)) {
		if (!empty($offset_id))
			returnOffset($result, $offset_id, $offset, $conn);
		else {
			header("HTTP/1.1 400 Bad Request");
			die("I need id with that offset. I don\'t know which tweet was the last you got");
		}
	} else {
		returnTweets($result, $conn);
	}
	
	function returnOffset($result, $id, $offset, $conn) {
		$outp = "";
		$inOffset = False;
	
		if (!is_null($result)) {
			while($rs = $result->fetch_array(MYSQLI_ASSOC)) {					
				$date = date_create($rs["time"]);
				$bullshit_date = date_format($date, 'Y-m-d')."T".date_format($date, 'H:i:s')."Z";

				// Check if this tweet is in the offset
				if (($inOffset or $rs["id"] == $id) and ($offset > 0)) {
					$inOffset = True;
					$offset = $offset - 1;
				} else {
					$inOffset = False;
				}
				
				if ($inOffset) {
					if ($outp != "") {$outp .= ",";}
					$outp .= '{"id":"'  . $rs["id"] . '",';
					$outp .= '"content":"'   . iconv("ISO-8859-2//TRANSLIT", "UTF-8", $rs["content"])       . '",';
					$outp .= '"candidate_id":"'   . $rs["candidate_id"]        . '",';
					$outp .= '"time":"'   . $bullshit_date        . '",';
					$outp .= '"user_login":"'   . $rs["user_login"]        . '",';
					$outp .= '"user_name":"'   . $rs["user_name"]        . '",';
					$outp .= '"user_pic":"'   . $rs["user_pic"]        . '",';
					$outp .= '"type":"TWEET"}';
				}
			}
		}
	
		$outp ='{"records":['.$outp.']}';
		$conn->close();

		header("HTTP/1.1 200 OK");
		echo($outp);
	}

	function returnTweets($result, $conn) {
		$outp = "";
	
		if (!is_null($result)) {
			while($rs = $result->fetch_array(MYSQLI_ASSOC)) {				
				$date = date_create($rs["time"]);
				$bullshit_date = date_format($date, 'Y-m-d')."T".date_format($date, 'H:i:s')."Z";
				if ($outp != "") {$outp .= ",";}
				$outp .= '{"id":"'  . $rs["id"] . '",';
				$outp .= '"content":"'   . iconv("ISO-8859-2//TRANSLIT", "UTF-8", $rs["content"])       . '",';
				$outp .= '"candidate_id":"'   . $rs["candidate_id"]        . '",';
				$outp .= '"time":"'   . $bullshit_date        . '",';
				$outp .= '"user_login":"'   . $rs["user_login"]        . '",';
				$outp .= '"user_name":"'   . $rs["user_name"]        . '",';
				$outp .= '"user_pic":"'   . $rs["user_pic"]        . '",';
				$outp .= '"type":"TWEET"}';
			}
		}
	
		$outp ='{"records":['.$outp.']}';
		$conn->close();

		header("HTTP/1.1 200 OK");
		echo($outp);
	}
	
	
	
?>
