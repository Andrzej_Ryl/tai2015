<?php
/*
DESC => Returns only one candidate
=================================
URL => twojkandydat.comxa.com/PHP/getCandidate.php
=================================
Query parameters =>
	id: Give me candidate with this id
=================================
JSON as input => NONE
=================================
Response =>
	HTTP 200 => {records: [{Name: string,
							Surname: string,
							Party: string,
							Age: string,
							Description: string,
							TwitterID: string,
							Image: string}]}
	HTTP 400 => Supply ID!
*/

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

	$id = $_GET['id'];

	if (empty($id)) {
		header("HTTP/1.1 400 Bad request");
		die('Supply ID!');
	}

	$conn = new mysqli("eryzim.nazwa.pl:3307", "eryzim", "Andrzejryl2015", "eryzim");
	// Get candidate with the given id
	$result = $conn->query("SELECT * FROM Candidates
							WHERE id = ".$id);

	if(!$result) {
		header("HTTP/1.1 500 Internal Server Error");
		die(mysqli_error($conn));
	}

	$outp = "";
	while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
		if ($outp != "") {$outp .= ",";}
		$outp .= '{"Name":"'  . $rs["name"] . '",';
		$outp .= '"Surname":"'   . $rs["surname"]        . '",';
		$outp .= '"Party":"'   . $rs["party"]        . '",';
		$outp .= '"Age":"'   . $rs["age"]        . '",';
		$outp .= '"Description":"'   . $rs["description"]        . '",';
		$outp .= '"TwitterID":"'   . $rs["twitterID"]        . '",';
		$outp .= '"Id":"'   . $id        . '",';
		$outp .= '"Image":"'. $rs["image"]     . '"}';
	}
	$outp ='{"records":['.$outp.']}';
	$conn->close();

	header("HTTP/1.1 200 OK");
	echo($outp);

?>
