<?php
/*
DESC => Returns all candidates
=================================
URL => twojkandydat.comxa.com/PHP/getCandidates.php
=================================
Query parameters => 
	event_id	: Return all candidates connected to this event
=================================
JSON as input => NONE
=================================
Response =>
	HTTP 200 => {records: [{Name: string,
							Surname: string,
							Id: string,
							Color: string,
							Image: string},
							{Name: string,
							Surname: string,
							Id: string,
							Color: string,
							Image: string},
							...]}
	HTTP 500 => SQL error
*/

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

	$conn = new mysqli("eryzim.nazwa.pl:3307", "eryzim", "Andrzejryl2015", "eryzim");

	$event_id = $_GET['event_id'];

	if (!empty($event_id)) {
		$result = $conn->query("SELECT * FROM Candidates
								JOIN CandidatesToEvents as x ON x.event_id = ".$event_id);
							
	} else {
		// Get everything from Candidates table
		$result = $conn->query("SELECT * FROM Candidates");
	}
	if(!$result) {
		header("HTTP/1.1 500 Internal Server Error");
		die(mysqli_error($conn));
	}
	
	$outp = "";
	while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
		if (!empty($event_id))
			$candidate_id = $rs["candidate_id"];
		else
			$candidate_id = $rs["id"];
		if ($outp != "") {$outp .= ",";}
		$outp .= '{"Name":"'  . $rs["name"] . '",';
		$outp .= '"Surname":"'   . $rs["surname"]        . '",';
		$outp .= '"Id":"'   . $candidate_id       . '",';
		$outp .= '"Color":"'   . $rs["color"]        . '",';
		$outp .= '"Image":"'. $rs["image"]     . '"}';
	}
	$outp ='{"records":['.$outp.']}';
	$conn->close();

	header("HTTP/1.1 200 OK");
	echo($outp);
?>
