<?php
/*

DESC => Returns events filtered by query parameters. It returns all kinds of events
		which means that in result we will have all debates, votings, rallies, tweets that
		are meeting query parameters.
=================================
URL => twojkandydat.comxa.com/PHP/getEvents.php
=================================
Query parameters =>
	id			: Give me an event with this id
	month		: Give me all events from this month (this parameter is an int 1-12)
	limit		: Give me last limit events (for every category)
	candidate_id: Give me events connected to this candidate
=================================
JSON as input => NONE
=================================
Response =>
	HTTP 200 => {records: [{id: int,
							TVname: string,
							description: string,
							host: string,
							candidate_id: int,
							location: string,
							time: datetime,
							type: string (= DEBATE)},
							OR
							{id: int,
							description: string,
							candidate_id: int,
							location: string,
							time: datetime,
							type: string (= VOTING)},
							OR
							{id: int,
							description: string,
							candidate_id: int,
							location: string,
							time: datetime,
							type: string (= RALLIES)},
							...
							]}
	HTTP 400 => Supply at least one of parameters! I won't return all of freaking events!
	HTTP 404 => There is no event with that ID. Maybe you wanted tweet?
*/

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

	$id = $_GET['id'];
	$month = $_GET['month'];
	$candidate_id = $_GET['candidate_id'];
	$limit = $_GET['limit'];
	$conn = new mysqli("eryzim.nazwa.pl:3307", "eryzim", "Andrzejryl2015", "eryzim");

	// Build queries
	$debate_query = "SELECT d.id as id, d.TVname as TVname, d.description as description,
							d.host as host, x.candidate_id as candidate_id, x.location as location, x.time as time FROM Debates as d
							JOIN CandidatesToEvents as x ON x.event_id = d.id ";
	$votings_query = "SELECT v.id as id, v.description as description,
							x.candidate_id as candidate_id, x.location as location, x.time as time FROM Votings as v
							JOIN CandidatesToEvents as x ON x.event_id = v.id ";
	$rallies_query = "SELECT r.id as id, r.description as description,
							x.candidate_id as candidate_id, x.location as location, x.time as time FROM ElectionRallies as r
							JOIN CandidatesToEvents as x ON x.event_id = r.id ";

	if (!empty($id)) {
		// We just need one tweet
		$debate_query = $debate_query . " WHERE d.id = " . $id;
		$votings_query = $votings_query . " WHERE v.id = " . $id;
		$rallies_query = $rallies_query . " WHERE r.id = " . $id;
	}
	else if (!empty($candidate_id)) {
		// Filter out events by parameters
		if (!empty($month)) {
			$debate_query = $debate_query . " WHERE x.candidate_id = " . $candidate_id . " AND MONTH(x.time) = ".$month;
			$votings_query = $votings_query . " WHERE x.candidate_id = " . $candidate_id . " AND MONTH(x.time) = ".$month;
			$rallies_query = $rallies_query . " WHERE x.candidate_id = " . $candidate_id . " AND MONTH(x.time) = ".$month;
		} else {
			$debate_query = $debate_query . " WHERE x.candidate_id = " . $candidate_id;
			$votings_query = $votings_query . " WHERE x.candidate_id = " . $candidate_id;
			$rallies_query = $rallies_query . " WHERE x.candidate_id = " . $candidate_id;
		}
	} else if (!empty($month)) {
		// Filter out events by parameters
		if (!empty($candidate_id)) {
			$debate_query = $debate_query . " WHERE x.candidate_id = " . $candidate_id . " AND MONTH(x.time) = ".$month;
			$votings_query = $votings_query . " WHERE x.candidate_id = " . $candidate_id . " AND MONTH(x.time) = ".$month;
			$rallies_query = $rallies_query . " WHERE x.candidate_id = " . $candidate_id . " AND MONTH(x.time) = ".$month;
		} else {
			$debate_query = $debate_query . " WHERE MONTH(x.time) = ".$month;
			$votings_query = $votings_query . " WHERE MONTH(x.time) = ".$month;
			$rallies_query = $rallies_query . " WHERE MONTH(x.time) = ".$month;
		}
	}
	else if (empty($id) && empty($limit) && empty($candidate_id)) {
		header("HTTP/1.1 400 Bad Request");
		die("Supply at least one of parameters! I won't return all of freaking events!");
	}


	// Add ordering and limit
	$debate_query = $debate_query . " ORDER BY time desc ";
	$votings_query = $votings_query . " ORDER BY time desc ";
	$rallies_query = $rallies_query . " ORDER BY time desc ";

	if (!empty($limit)) {
		$debate_query = $debate_query  . " LIMIT " . $limit;
		$votings_query = $votings_query  . " LIMIT " . $limit;
		$rallies_query = $rallies_query  . " LIMIT " . $limit;
	}

	if (!empty($id)) {
		// Check the type of event
		$result = $conn->query("SELECT COUNT(*) FROM Debates
								WHERE id = ".$id);
		$isDebate = $result->fetch_array(MYSQLI_ASSOC);
		$isDebate = ($isDebate["COUNT(*)"] == 1);

		$result = $conn->query("SELECT COUNT(*) FROM Votings
								WHERE id = ".$id);
		$isVoting = $result->fetch_array(MYSQLI_ASSOC);
		$isVoting = ($isVoting["COUNT(*)"] == 1);

		$result = $conn->query("SELECT COUNT(*) FROM ElectionRallies
								WHERE id = ".$id);
		$isElection = $result->fetch_array(MYSQLI_ASSOC);
		$isElection = ($isElection["COUNT(*)"] == 1);

		if (!$isDebate && !$isVoting && !$isElection) {
			header("HTTP/1.1 404 Resource not found");
			die("There is no event with that ID. Maybe you wanted tweet?");
		}
	}

	// Get results from every table
	$debate_result = $conn->query($debate_query);
	$votings_result = $conn->query($votings_query);
	$rallies_result = $conn->query($rallies_query);

	if(!$debate_result or !$votings_result or !$rallies_result) {
		header("HTTP/1.1 500 Internal Server Error");
		die(mysqli_error($conn));
	}

	returnEvents($debate_result, $votings_result, $rallies_result, $conn);

	function returnEvents($resultDebates, $resultVotings, $resultElections, $conn) {
		$outp = "";
		if (!is_null($resultDebates)) {
			while($rs = $resultDebates->fetch_array(MYSQLI_ASSOC)) {
				$date = date_create($rs["time"]);
				$bullshit_date = date_format($date, 'Y-m-d')."T".date_format($date, 'H:i:s')."Z";
				if ($outp != "") {$outp .= ",";}
				$outp .= '{"id":"'  . $rs["id"] . '",';
				$outp .= '"TVname":"'   . $rs["TVname"]        . '",';
				$outp .= '"description":"'   . $rs["description"]        . '",';
				$outp .= '"host":"'   . $rs["host"]        . '",';
				$outp .= '"candidate_id":"'   . $rs["candidate_id"]        . '",';
				$outp .= '"location":"'   . $rs["location"]        . '",';
				$outp .= '"time":"'   . $bullshit_date        . '",';
				$outp .= '"type":"DEBATE"}';
			}
		}

		if (!is_null($resultVotings)) {
			while($rs = $resultVotings->fetch_array(MYSQLI_ASSOC)) {
				$date = date_create($rs["time"]);
				$bullshit_date = date_format($date, 'Y-m-d')."T".date_format($date, 'H:i:s')."Z";
				if ($outp != "") {$outp .= ",";}
				$outp .= '{"id":"'  . $rs["id"] . '",';
				$outp .= '"description":"'   . $rs["description"]        . '",';
				$outp .= '"candidate_id":"'   . $rs["candidate_id"]        . '",';
				$outp .= '"location":"'   . $rs["location"]        . '",';
				$outp .= '"time":"'   . $bullshit_date        . '",';
				$outp .= '"type":"VOTING"}';
			}
		}

		if (!is_null($resultElections)) {
			while($rs = $resultElections->fetch_array(MYSQLI_ASSOC)) {
				$date = date_create($rs["time"]);
				$bullshit_date = date_format($date, 'Y-m-d')."T".date_format($date, 'H:i:s')."Z";
				if ($outp != "") {$outp .= ",";}
				$outp .= '{"id":"'  . $rs["id"] . '",';
				$outp .= '"description":"'   . $rs["description"]        . '",';
				$outp .= '"candidate_id":"'   . $rs["candidate_id"]        . '",';
				$outp .= '"location":"'   . $rs["location"]        . '",';
				$outp .= '"time":"'   . $bullshit_date        . '",';
				$outp .= '"type":"RALLIES"}';
			}
		}
		$outp ='{"records":['.$outp.']}';
		$conn->close();

		header("HTTP/1.1 200 OK");
		echo($outp);
	}

?>
