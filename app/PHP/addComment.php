<?php
/*
DESC => Adds a comment
=================================
URL => twojkandydat.comxa.com/PHP/addComment.php
=================================
Query parameters => 
	id 		: Update comment with this id
=================================
JSON as input => {event_id: string,
				nickname: string,
				content: string,
				date: datetime,
				user_id: string}
=================================
Response => 
	HTTP 200 => Success
	HTTP 500 => SQL error
*/
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

	/*
	* Collect all Details from Angular HTTP Request.
	*/ 
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$event_id = $request->event_id;
	$nickname = $request->nickname;
	$content = $request->content;
	$date = $request->date;
	$user_id = $request->user_id;

	$conn = new mysqli("eryzim.nazwa.pl:3307", "eryzim", "Andrzejryl2015", "eryzim");
	
	$id = $_GET["id"];
	
	if (!empty($id)) {
		// Update this comment
		$query = "UPDATE Comments
							SET event_id = ".$event_id.", nickname = '".$nickname."', content = '".$content."', date = '".$date."', user_id = '".$user_id."'
							 WHERE id = ".$id;
		$result = $conn->query($query);
	} else {
		// Insert a record to the table
		$result = $conn->query("INSERT INTO Comments (
							event_id, nickname, content, date, user_id)
							VALUES ('".$event_id."','".$nickname."','".$content."','".$date."','".$user_id."')");
	}

	$conn->close();

	if (!$conn || !$result) {
		header("HTTP/1.1 500 Internal Server Error");
		echo mysqli_error($conn);
	} else {
		header("HTTP/1.1 200 OK");
		echo "Success";
	}

?>
