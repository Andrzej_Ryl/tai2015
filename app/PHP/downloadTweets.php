<?php
	/*
		DESC => This methods downloads fresh tweets to our database.
			It downloads 5 newest tweets for each candidate and updates them in DB.
			SO in our DB there are always 55 tweets on the first 55 indices.
	*/

	error_reporting(E_ALL);
	ini_set('display_errors', 1);

	require "twitteroauth-0.5.3/autoload.php";
	use Abraham\TwitterOAuth\TwitterOAuth;
	
	# Define constants
	define('TWEET_LIMIT', 200);
	define('TWITTER_USERNAME', 'eran_tai_2015');
	define('CONSUMER_KEY', 'p4lJOJVNivw51MaKqFFiCNWZp');
	define('CONSUMER_SECRET', 'Uuizc1L0pggYLYeMLy3irocBhKgxEcsEuKxW69xrLfQx99Wk3N');
	define('ACCESS_TOKEN', '3319354642-yxKmFvriA0YgIiWhMx8SG0kHsTOg7O7RHQAjZHF');
	define('ACCESS_TOKEN_SECRET', 'Ityp9q3dD2QTJzjSYjZhTpdlUWNHTugtPFg0aO7ZA9IIT');

	# Create the connection
	$twitter = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);

	# Migrate over to SSL/TLS
	$twitter->ssl_verifypeer = true;
	
	# Load list of candidates with their twitterIDs
	$conn = new mysqli("eryzim.nazwa.pl:3307", "eryzim", "Andrzejryl2015", "eryzim");
	$result = $conn->query("SELECT id,twitterID FROM Candidates");
	
	if(!$result) {
		header("HTTP/1.1 500 Internal Server Error");
		die(mysqli_error($result));
	}
	
	$outp = "";

	if (!is_null($result)) {
		while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
			if ($outp != "") {$outp .= ",";}
			$outp .= '{"twitterID":"'  . $rs["twitterID"] .  '",';
			$outp .= '"candidate_id":"'.$rs["id"].'"}';
		}
	}

	$outp ='{"records":['.$outp.']}';

	# Load the Tweets for every candidate (5 of them)
	$result = array();
	$twitterResults = array();
	
	foreach (json_decode($outp,true)["records"] as $row) {
		# Download 200 tweets (I have to do it like that because 'count' parameter in TwitterAPI 
		# is connected also to replies which we don't need
		$twitterResult = $twitter->get('statuses/user_timeline', array('screen_name' => $row["twitterID"], 'exclude_replies' => 'true', 'include_rts' => 		'true',	'count' => TWEET_LIMIT));
		
		# Take first 5 
		$twitterResults[$row["twitterID"]] = array_slice($twitterResult,0,5);
	}
	
	# Get only texts and creation times from those tweets
	foreach ($twitterResults as $twitterID => $twitterResult) {
		if(!empty($twitterResult)) {
			$tweets = array();
			foreach($twitterResult as $tweet) {
				$tweet = (array)$tweet;
				# Access as an object
				$tweetText = iconv("UTF-8", "ISO-8859-2//TRANSLIT", $tweet['text']);

				# Add to our array
				$user = (array)$tweet["user"];
				$row = Array("content" => $tweetText, 
							"time" => $tweet["created_at"], 
							"screen_name" =>$twitterID,
							"name" => $user["name"],
							"pic" => $user["profile_image_url"]);
				array_push($tweets, $row);
			}
			
			# Build result
			$result[$twitterID] = $tweets;
		}
	}
	
	# Load those tweets into DB

	$id = 1;
	foreach ($result as $twitterID => $tweets) {
		$candidateID = getCandidateID($twitterID);
		foreach($tweets as $tweet) {
			$time = new DateTime($tweet["time"]);
			$query = "INSERT INTO Tweets (id, content, candidate_id, time, user_login, user_name, user_pic)
				VALUES (".$id.",'".$tweet["content"]."', ".$candidateID.",'".$time->format('Y-m-d H:i:s')."
						', '".$tweet["screen_name"]."', '".$tweet["name"]."', '".$tweet["pic"]."') 
				ON DUPLICATE KEY UPDATE 
					content = '".$tweet["content"]."', 
					time = '".$time->format('Y-m-d H:i:s')."'";
					
			$insert_result = $conn->query($query);
			
			if (!$insert_result) {
				header("HTTP/1.1 500 Internal Server Error");
				echo mysqli_error($conn);
			}
			
			$id += 1;
				
		}
	}
	$conn->close();

	if (!$conn) {
		header("HTTP/1.1 500 Internal Server Error");
		echo mysqli_error($conn);
	} else {
		header("HTTP/1.1 200 OK");
		echo "Success";
	}
	
	function getCandidateID($twitterID) { 
		global $outp;
		$array = json_decode($outp, true);
		
		foreach($array["records"] as $row) {
			if ($row["twitterID"] == $twitterID)
				return $row["candidate_id"];
		}
	}
?>