<?php
/*
DESC => Returns 2 debates, 2 votings, 2 rallies and 22 tweets (2 tweets for each candidate)
	sorted by date

URL => twojkandydat.comxa.com/PHP/getTimeline.php
=================================
Query parameters => NONE
=================================
JSON as input => NONE
=================================
Response =>
	HTTP 200 => {records: [{id: int,
							TVname: string,
							description: string,
							host: string,
							candidate_id: int,
							location: string,
							time: datetime,
							type: string (= event)},
							OR
							{id: int,
							description: string,
							candidate_id: int,
							location: string,
							time: datetime,
							type: string (= event)},
							OR
							{id: int,
							description: string,
							candidate_id: int,
							location: string,
							time: datetime,
							type: string (= event)},
							OR
							{id: int,
							content: string,
							candidate_id: int,
							user_login: string,
							user_name: string,
							user_pic: string,
							time: datetime,
							type: string (= tweet)},
							...
							]}
*/

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

	$conn = new mysqli("eryzim.nazwa.pl:3307", "eryzim", "Andrzejryl2015", "eryzim");	// Return events (22 tweets and 6 events of every type)
	$resultDebates = $conn->query("SELECT d.id as id, d.TVname as TVname, d.description as description,
					d.host as host, x.candidate_id as candidate_id, x.location as location, x.time as time FROM Debates as d
					JOIN CandidatesToEvents as x ON x.event_id = d.id
					ORDER BY x.time LIMIT 2");

	$resultVotings = $conn->query("SELECT v.id as id, v.description as description,
					x.candidate_id as candidate_id, x.location as location, x.time as time FROM Votings as v
					JOIN CandidatesToEvents as x ON x.event_id = v.id
					ORDER BY x.time LIMIT 2");

	$resultElections = $conn->query("SELECT r.id as id, r.description as description,
					x.candidate_id as candidate_id, x.location as location, x.time as time FROM ElectionRallies as r
					JOIN CandidatesToEvents as x ON x.event_id = r.id
					ORDER BY x.time LIMIT 2");

	$resultTweets = $conn->query("SELECT id, content, candidate_id, time, user_login, user_name, user_pic FROM Tweets
					ORDER BY time LIMIT 22");


	if(!$resultDebates || !$resultVotings || !$resultElections || !$resultTweets) {
		header("HTTP/1.1 500 Internal Server Error");
		die(mysqli_error($conn));
	}

	returnTimeline($resultDebates, $resultVotings, $resultElections, $resultTweets, $conn);



	function returnTimeline($resultDebates, $resultVotings, $resultElections, $resultTweets, $conn) {
		$outp = "";
		if (!is_null($resultDebates)) {
			while($rs = $resultDebates->fetch_array(MYSQLI_ASSOC)) {
				$date = date_create($rs["time"]);
				$bullshit_date = date_format($date, 'Y-m-d')."T".date_format($date, 'H:i:s')."Z";
		
				if ($outp != "") {$outp .= ",";}
				$outp .= '{"id":"'  . $rs["id"] . '",';
				$outp .= '"TVname":"'   . $rs["TVname"]        . '",';
				$outp .= '"description":"'   . $rs["description"]        . '",';
				$outp .= '"host":"'   . $rs["host"]        . '",';
				$outp .= '"candidate_id":"'   . $rs["candidate_id"]        . '",';
				$outp .= '"location":"'   . $rs["location"]        . '",';
				$outp .= '"time":"'   . $bullshit_date . '",';
				$outp .= '"type":"event"}';
			}
		}

		if (!is_null($resultVotings)) {
			while($rs = $resultVotings->fetch_array(MYSQLI_ASSOC)) {
				$date = date_create($rs["time"]);
				$bullshit_date = date_format($date, 'Y-m-d')."T".date_format($date, 'H:i:s')."Z";
		
				if ($outp != "") {$outp .= ",";}
				$outp .= '{"id":"'  . $rs["id"] . '",';
				$outp .= '"description":"'   . $rs["description"]        . '",';
				$outp .= '"candidate_id":"'   . $rs["candidate_id"]        . '",';
				$outp .= '"location":"'   . $rs["location"]        . '",';
				$outp .= '"time":"'   . $bullshit_date        . '",';
				$outp .= '"type":"event"}';
			}
		}

		if (!is_null($resultElections)) {
			while($rs = $resultElections->fetch_array(MYSQLI_ASSOC)) {
				$date = date_create($rs["time"]);
				$bullshit_date = date_format($date, 'Y-m-d')."T".date_format($date, 'H:i:s')."Z";
		
				if ($outp != "") {$outp .= ",";}
				$outp .= '{"id":"'  . $rs["id"] . '",';
				$outp .= '"description":"'   . $rs["description"]        . '",';
				$outp .= '"candidate_id":"'   . $rs["candidate_id"]        . '",';
				$outp .= '"location":"'   . $rs["location"]        . '",';
				$outp .= '"time":"'   . $bullshit_date        . '",';
				$outp .= '"type":"event"}';
			}
		}

		if (!is_null($resultTweets)) {
			while($rs = $resultTweets->fetch_array(MYSQLI_ASSOC)) {
				$date = date_create($rs["time"]);
				$bullshit_date = date_format($date, 'Y-m-d')."T".date_format($date, 'H:i:s')."Z";
		
				if ($outp != "") {$outp .= ",";}
				$outp .= '{"id":"'  . $rs["id"] . '",';
				$outp .= '"content":"'   . iconv("ISO-8859-2//TRANSLIT", "UTF-8", $rs["content"])       . '",';
				$outp .= '"candidate_id":"'   . $rs["candidate_id"]        . '",';
				$outp .= '"user_login":"'   . $rs["user_login"]        . '",';
				$outp .= '"user_name":"'   . $rs["user_name"]        . '",';
				$outp .= '"user_pic":"'   . $rs["user_pic"]        . '",';
				$outp .= '"time":"'   . $bullshit_date        . '",';
				$outp .= '"type":"tweet"}';
			}
		}

		$outp ='{"records":['.$outp.']}';
		$conn->close();

		header("HTTP/1.1 200 OK");
		echo($outp);
	}



?>
