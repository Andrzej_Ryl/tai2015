'use strict';

/**
 * @ngdoc overview
 * @name twojkandydat2015App
 * @description
 * # twojkandydat2015App
 *
 * Main module of the application.
 */

var app = angular
  .module('twojkandydat2015App',
		['auth0',
		'angular-storage',
		'angular-jwt']);

  app.config(function ($routeProvider, authProvider, $httpProvider, $locationProvider,
			jwtInterceptorProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/tweets', {
        templateUrl: 'views/tweets.html',
        controller: 'TweetsCtrl'
      })
      .when('/tweets/:tweetId', {
        templateUrl: 'views/tweet.html',
        controller: 'TweetCtrl'
      })
      .when('/channels/:candidateId', {
        templateUrl: 'views/tweets.html',
        controller: 'ChannelCtrl'
      })
      .when('/timeline', {
        templateUrl: 'views/timeline.html',
        controller: 'TimelineCtrl'
      })
      .when('/candidates', {
        templateUrl: 'views/candidates.html',
        controller: 'CandidatesCtrl'
      })
      .when('/candidates/:candidateId', {
        templateUrl: 'views/candidate.html',
        controller: 'CandidateCtrl'
      })
      .when('/calendar', {
        templateUrl: 'views/calendar.html',
        controller: 'CalendarCtrl'
      })
      .when('/day', {
        templateUrl: 'views/day.html',
        controller: 'DayCtrl'
      })
      .when('/comment', {
        templateUrl: 'views/comment.html',
        controller: 'CommentCtrl'
      })
      .when('/events', {
        templateUrl: 'views/calendar.html',
        controller: 'CalendarCtrl'
      })
      .when('/events/:eventId', {
        templateUrl: 'views/event.html',
        controller: 'EventCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });


	  // AUTH ADDITIONS
	  authProvider.init({
		domain: AUTH0_DOMAIN,
		clientID: AUTH0_CLIENT_ID,
		loginUrl: '/login'
	  });

	  jwtInterceptorProvider.tokenGetter = function(store) {
		  return store.get('token');
	  }
  })
  .run(function($rootScope, auth, store, jwtHelper) {
    // This events gets triggered on refresh or URL change
    $rootScope.$on('$locationChangeStart', function() {
      var token = store.get('token');
      if (token) {
        if (!jwtHelper.isTokenExpired(token)) {
          if (!auth.isAuthenticated) {
            auth.authenticate(store.get('profile'), token);
          }
        } else {
          login(auth, store);
        }
      }
    });
  });


