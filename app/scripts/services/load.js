/**
 * Created by Eryk on 2015-06-29.
 */

app.service('loadMoreService', ['$http',function($http){
    this.loadMore = function(url,offset,limit){
      url += '?offset='+offset;
      url += '&limit='+limit;
      $http.get(url)
        .success(function(response){
          console.log(response.records);
          return response.records;
        })
        .error(function(response){
          console.log(response);
          return {error: response};
        });
    }
}]);
