/**
 * Created by Eryk on 2015-06-29.
 */

app.service('dayEventsService', function(){
    var currentEvents,currentDay,currentMonth;

    this.setEvents = function(events,day,month){
      currentEvents = events;
      currentDay = day;
      currentMonth = month;

    };

    this.getEvents = function(){
      return {
        events: currentEvents,
        day: currentDay,
        month: currentMonth
      };
    };
});
