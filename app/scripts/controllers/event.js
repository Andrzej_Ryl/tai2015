'use strict';

/**
 * @ngdoc function
 * @name twojkandydat2015App.controller:EventCtrl
 * @description
 * # EventCtrl
 * Controller of the twojkandydat2015App
 */
angular.module('twojkandydat2015App')
  .controller('EventCtrl',['$scope','$http','$routeParams',function ($scope,$http,$routeParams) {
    //$scope.events = [
    //  {
    //    "id": "1",
    //    "TVname": "d",
    //    "description": "d",
    //    "host": "d",
    //    "candidate_id": "1",
    //    "location": "d",
    //    "time": "2015-07-02T20:47:34Z",
    //    "type": "DEBATE"
    //  },
    //  {
    //    "id": "2",
    //    "description": "d",
    //    "candidate_id": "1",
    //    "location": "f",
    //    "time": "2015-07-02T20:47:34Z",
    //    "type": "VOTING"
    //  },
    //  {
    //    "id": "3",
    //    "description": "d",
    //    "candidate_id": "1",
    //    "location": "f",
    //    "time": "2015-07-02T20:47:34Z",
    //    "type": "RALLIES"
    //  }
    //];
    //
    //$scope.candidates = [
    //  {
    //    Name: 'Bronisław',
    //    Surname: 'Komorowski',
    //    Image: 'http://cdn.nowiny.pl/im/v1/news-300x300-crop/2013/09/13/93649.jpg',
    //    Color: '#ED7600',
    //    Id: 0
    //  }
    //];

    var convertDates = function(){
      $scope.event.time = new Date($scope.event.time);
    };

    // After merge we use tweet from database

    $scope.eventId =  $routeParams.eventId;

    $http.get('http://eryzim.nazwa.pl/twojkandydat/getEvents.php?id='+$scope.eventId)
      .success(function(response){
        console.log(response.records);
        var events = response.records;
        $scope.event = events[0];
        convertDates();
      })
      .error(function(response){
        console.log(response);
      });

    $http.get('http://eryzim.nazwa.pl/twojkandydat/getCandidates.php?event_id='+$scope.eventId)
      .success(function(response){
        console.log(response.records);
        $scope.candidates = response.records;
      })
      .error(function(response){
        console.log(response);
      });

     $scope.commentableID = 1;
  }]);
