'use strict';

/**
 * @ngdoc function
 * @name twojkandydat2015App.controller:CandidateCtrl
 * @description
 * # CandidateCtrl
 * Controller of the twojkandydat2015App
 */
angular.module('twojkandydat2015App')
  .controller('CandidateCtrl',['$scope','$http','$routeParams',function ($scope,$http,$routeParams) {
    $scope.candidates = [
      {"Name":"test","Surname":"test","Party":"test","Age":"18","Description":"test","TwitterID":"AndrzejDuda","Id":"1","Image":"http://cdn.nowiny.pl/im/v1/news-300x300-crop/2013/09/13/93649.jpg"}
    ];

    $scope.candidateId =  $routeParams.candidateId;
    //$scope.candidate = $scope.candidates[$scope.candidateId];

    // The get request will be tested after merge with backend

    $http.get('http://eryzim.nazwa.pl/twojkandydat/getCandidate.php?id=' + $scope.candidateId)
      .success(function(response){
        console.log(response.records);
        $scope.candidate = response.records[0];
      })
      .error(function(response){
        console.log(response);
      });
  }]);
