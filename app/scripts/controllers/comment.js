'use strict';

/**
 * @ngdoc function
 * @name twojkandydat2015App.controller:CommentCtrl
 * @description
 * # CommentCtrl
 * Controller of the twojkandydat2015App
 */
angular.module('twojkandydat2015App')
  .controller('CommentCtrl', ['$scope', 'auth', '$location', 'store','$http',function ($scope, auth, $location, store,$http) {

    //$scope.comments = [
    //  {"id":"1","event_id":"1","nickname":"Maciek Sroka","content":"Nie xd nie wieże ze nie masz znajomych z którymi mogła byś się udać np na rynek wink emoticon chodź z tym zaczepianiem mogło by być ciekawie ^^","date":"2015-06-12T00:00:00Z","user_id":""},
    //  {"id":"2","event_id":"1","nickname":"Maciek Sroka","content":"Nie xd nie wieże ze nie masz znajomych z którymi mogła byś się udać np na rynek wink emoticon chodź z tym zaczepianiem mogło by być ciekawie ^^","date":"2015-06-12T00:00:00Z","user_id":""},
    //  {"id":"3","event_id":"1","nickname":"Maciek Sroka","content":"Nie xd nie wieże ze nie masz znajomych z którymi mogła byś się udać np na rynek wink emoticon chodź z tym zaczepianiem mogło by być ciekawie ^^","date":"2015-06-12T00:00:00Z","user_id":""},
    //  {"id":"4","event_id":"1","nickname":"Maciek Sroka","content":"Nie xd nie wieże ze nie masz znajomych z którymi mogła byś się udać np na rynek wink emoticon chodź z tym zaczepianiem mogło by być ciekawie ^^","date":"2015-06-12T00:00:00Z","user_id":""},
    //  {"id":"5","event_id":"1","nickname":"Maciek Sroka","content":"Nie xd nie wieże ze nie masz znajomych z którymi mogła byś się udać np na rynek wink emoticon chodź z tym zaczepianiem mogło by być ciekawie ^^","date":"2015-06-12T00:00:00Z","user_id":""}
    //];



    var modifyDate = function(events){
      events.forEach(function(entry){
        entry.date = new Date(entry.date);
        entry.owner = auth.isAuthenticated && auth.profile.user_id == entry.user_id;
      });
    };

    $http.get('http://eryzim.nazwa.pl/twojkandydat/getComments.php?eventID='+$scope.eventId)
      .success(function(response){
        console.log(response.records);
        $scope.comments = response.records.reverse();
        modifyDate($scope.comments);
      })
      .error(function(response){
        console.log(response);
      });


    $scope.edited = null;
    $scope.isAdmin = auth.isAuthenticated && auth.profile.user_metadata.type == "admin";


    $scope.addComment = function(content){
      if (auth.isAuthenticated) {
        if($scope.edited){
          $scope.edited.content = content;
          $http.post('http://eryzim.nazwa.pl/twojkandydat/addComment.php?id='+$scope.edited.id, data)
            .success(function (response) {
              $scope.comments.unshift(comment);
              $scope.edited = null;
              document.getElementById('comment').value = "";
            })
            .error(function (response) {
              console.log(response);
            });

        } else {
          var date = new Date();
          var data = {
            event_id: $scope.eventId,
            nickname: auth.profile.email,
            content: content,
            date: date.toISOString(),
            user_id: auth.profile.user_id
          };
          var comment = data;
          comment.date = date;
          comment.owner = true;

          $http.post('http://eryzim.nazwa.pl/twojkandydat/addComment.php', data)
            .success(function (response) {
              $scope.comments.unshift(comment);
              $scope.newComment = "";
              document.getElementById('comment').value = "";
            })
            .error(function (response) {
              console.log(response);
            });
        }
      } else {
        // Show the user login widget
        login(auth, store);
      }
    };

    $scope.deleteComment = function(id,index){
      $http.get('http://eryzim.nazwa.pl/twojkandydat/deleteComment.php?id='+id)
        .success(function(response){
          console.log(response.records);
          $scope.comments.splice(index, 1);
        })
        .error(function(response){
          console.log(response);
        });
    };

    $scope.editComment = function(index,content){
      $scope.edited = $scope.comments.splice(index, 1);

      document.getElementById('comment').value = content;
    };




  }]);
