'use strict';

/**
 * @ngdoc function
 * @name twojkandydat2015App.controller:CalendarCtrl
 * @description
 * # CalendarCtrl
 * Controller of the twojkandydat2015App
 */
angular.module('twojkandydat2015App')
  .controller('DayCtrl', ['$scope','$http','dayEventsService','$location',function ($scope,$http,dayEventsService,$location) {

    $scope.day = dayEventsService.getEvents();

    if(!$scope.day.events || $scope.day.events.length === 0){
      $location.path('/calendar');
      $location.replace();
    }
  }]);
