'use strict';

/**
 * @ngdoc function
 * @name twojkandydat2015App.controller:CalendarCtrl
 * @description
 * # CalendarCtrl
 * Controller of the twojkandydat2015App
 */
angular.module('twojkandydat2015App')
  .controller('CalendarCtrl', ['$scope','$http','dayEventsService','$location',function ($scope,$http,dayEventsService,$location) {

    $scope.showingMonth = new Date().getMonth() + 1;
    $scope.showingYear = new Date().getYear();

    // Metody drukujące odpowiednie słowa

    $scope.printEventText = function(number){
      if(number === 1){
        return "wydarzenie";
      } else if(number < 5) {
        return "wydarzenia";
      } else {
        return "wydarzeń";
      }
    };

    $scope.currentMonth = function(){
      return ['Styczeń','Luty','Marzec',
        'Kwiecień','Maj','Czerwiec',
        'Lipiec','Sierpień','Wrzesień',
        'Październik','Listopad','Grudzień'][$scope.showingMonth-1];
    };


    $scope.currentMonthByDay = function(){
      return ['Stycznia','Lutego','Marca',
        'Kwietnia','Maja','Czerwca',
        'Lipca','Sierpnia','Września',
        'Października','Listopada','Grudnia'][$scope.showingMonth-1];
    };

    // Najważniejsza funkcja zmiany miesiąca

    $scope.changeMonth = function(direction){
      $scope.showingMonth += direction;
      if($scope.showingMonth === 0){
        $scope.showingYear -= 1;
        $scope.showingMonth = 12;
      } else if ($scope.showingMonth === 13){
        $scope.showingMonth = 1;
        $scope.showingYear += 1;
      }
      loadMonthEvents();

    };

    // Przekazujemy dni do kolejnego widoku

    $scope.showDayEvents = function(events,day){
      dayEventsService.setEvents(events,day,$scope.currentMonthByDay());
    };

    //$scope.events = [
    //  {
    //    "id": "1",
    //    "TVname": "d",
    //    "description": "d",
    //    "host": "d",
    //    "candidate_id": "1",
    //    "location": "d",
    //    "time": "2015-07-02T20:47:34Z",
    //    "type": "DEBATE"
    //  },
    //  {
    //    "id": "2",
    //    "description": "d",
    //    "candidate_id": "1",
    //    "location": "f",
    //    "time": "2015-07-02T20:47:34Z",
    //    "type": "VOTING"
    //  },
    //  {
    //    "id": "3",
    //    "description": "d",
    //    "candidate_id": "1",
    //    "location": "f",
    //    "time": "2015-07-02T20:47:34Z",
    //    "type": "RALLIES"
    //  }
    //];

    // Zwracamy ilość dni w miesiącu

    var dayInMonth = function(month,year) {
      return new Date(year, month, 0).getDate();
    };

    // Metoda do przerobienia dat

    var convertDates = function(){
      $scope.events.forEach(function(entry){
        entry.time = new Date(entry.time);
      });
    };

    var loadMonthEvents = function() {
      $http.get('http://eryzim.nazwa.pl/twojkandydat/getEvents.php?month=' + $scope.showingMonth)
        .success(function (response) {
          console.log(response.records);
          $scope.events = response.records;
          createCalendar()
        })
        .error(function (response) {
          console.log(response);
        });
    };

    // Metoda do tworzenia tablicy kalendarza

    var createCalendar = function() {

      // Zerujemy naszą tablicę kalendarza
      $scope.currentEvents = new Array(dayInMonth($scope.showingMonth,$scope.showingYear));
      for (var i = 0; i < dayInMonth($scope.showingMonth,$scope.showingYear); i++)
        $scope.currentEvents[i]=new Array(0)

      // Przerobienie dat
      convertDates();

      // Tworzenie tablicy kalendarza
      $scope.events.forEach(function(entry){
        if(entry.time) {
          $scope.currentEvents[entry.time.getDate()-1].push(entry);
        }
      });
    };


    var goTo = function(url){
      $location.path(url);
      $location.replace();
    };

    // Pierwsze wywołanie





    // events from backend after merge



    var w = $(window);

    var centerCalendar= function() {
      var width = $(window).width();
      width = width - ((width > 750) ? 350 : 100);
      var listsize = Math.floor(width / 100);

      if (listsize === 0) {
        listsize = 1;
      } else if (listsize > 12) {
        listsize = 12;
      }

      $scope.listsize = 102 * listsize;
      $('#calendar').css('max-width',$scope.listsize+'px');
    };

    centerCalendar();

    w.bind('resize', centerCalendar);

    loadMonthEvents();


  }]);
