'use strict';

/**
 * @ngdoc function
 * @name twojkandydat2015App.controller:TweetsCtrl
 * @description
 * # TweetsCtrl
 * Controller of the twojkandydat2015App
 */
angular.module('twojkandydat2015App')
  .controller('ChannelCtrl', ['$scope','$location','$routeParams','$http', function ($scope,$location,$routeParams,$http) {
    //$scope.tweets = [
    //  {
    //    "id": "1",
    //    "content": "RT @onetpl: O�wiadczenie @onetpl w sprawie spreparowanego materia�u na temat @andrzejduda kr���cego w mediach spo�eczno�ciowych: http://t.c...",
    //    "candidate_id": "1",
    //    "time": "2015-06-29T16:06:51Z",
    //    "user_login": "AndrzejDuda",
    //    "user_name": "Andrzej Duda",
    //    "user_pic": "http://pbs.twimg.com/profile_images/556495456805453826/wKEOCDN0_normal.png",
    //    "type": "TWEET"
    //  },
    //  {
    //    "id": "2",
    //    "content": "Polecam wakacyjn� lektur�. Bardzo sympatyczna i ciekawa opowie�� o dziejach rodziny: http://t.co/VpwBlMsPBf",
    //    "candidate_id": "1",
    //    "time": "2015-06-29T15:58:06Z",
    //    "user_login": "AndrzejDuda",
    //    "user_name": "Andrzej Duda",
    //    "user_pic": "http://pbs.twimg.com/profile_images/556495456805453826/wKEOCDN0_normal.png",
    //    "type": "TWEET"
    //  },
    //  {
    //    "id": "3",
    //    "content": "RT @MateuszWodecki: Prezydent elekt @AndrzejDuda spotka� si� dzisiaj z przedstawicielami @KoLiber_org @MlodzidlaPolski i @StudencidlaRP htt...",
    //    "candidate_id": "1",
    //    "time": "2015-06-23T15:51:38Z",
    //    "user_login": "AndrzejDuda",
    //    "user_name": "Andrzej Duda",
    //    "user_pic": "http://pbs.twimg.com/profile_images/556495456805453826/wKEOCDN0_normal.png",
    //    "type": "TWEET"
    //  },
    //  {
    //    "id": "4",
    //    "content": "RT @KoLiberWarszawa: Jak wida� humory dopisywa�y. Prezydent Elekt @AndrzejDuda razem z wiceprezesem @KoLiberWarszawa http://t.co/YaTS1Mw88G",
    //    "candidate_id": "1",
    //    "time": "2015-06-23T09:50:47Z",
    //    "user_login": "AndrzejDuda",
    //    "user_name": "Andrzej Duda",
    //    "user_pic": "http://pbs.twimg.com/profile_images/556495456805453826/wKEOCDN0_normal.png",
    //    "type": "TWEET"
    //  },
    //  {
    //    "id": "5",
    //    "content": "RT @jakubkowalski: Wielu mieszk. @RadomCity i regionu pracowa�o w kampanii @AndrzejDuda. Dzisiaj spotkali�my si�,by im podzi�kowa�. http://...",
    //    "candidate_id": "1",
    //    "time": "2015-06-12T18:35:36Z",
    //    "user_login": "AndrzejDuda",
    //    "user_name": "Andrzej Duda",
    //    "user_pic": "http://pbs.twimg.com/profile_images/556495456805453826/wKEOCDN0_normal.png",
    //    "type": "TWEET"
    //  }
    //];
    $scope.getMyCtrlScope = function() {
      return $scope;
    };

    var w = $(window);

    var positionPhone = function() {
      var height = $(window).height();
      height -= 165;
      $('#tweets').css('height',height+'px');
    };

    var checkReTweets = function(events) {
      events.forEach(function(entry){
        if(entry.content.substring(0,2) === "RT"){
          entry.content = "♻"+entry.content.slice(2,-1);
        }
      });
    };

    w.bind('resize', positionPhone);

    $scope.goTo = function(url){
      $location.path(url);
      $location.replace();
      console.log(url);
    }

    $scope.candidateId =  $routeParams.candidateId;

    $http.get('http://eryzim.nazwa.pl/twojkandydat/getTweets.php?candidate_id='+$scope.candidateId)
      .success(function(response){
        console.log(response.records);
        $scope.tweets = response.records;
        positionPhone();
        checkReTweets($scope.tweets);
      })
      .error(function(response){
        console.log(response);
      });

    // Base on candidateId get 5 latest tweets from database
    // For test we use predefined list of tweets


  }]);
