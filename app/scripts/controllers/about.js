'use strict';

/**
 * @ngdoc function
 * @name twojkandydat2015App.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the twojkandydat2015App
 */
angular.module('twojkandydat2015App')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
