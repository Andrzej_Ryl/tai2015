'use strict';

/**
 * @ngdoc function
 * @name twojkandydat2015App.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the twojkandydat2015App
 */
angular.module('twojkandydat2015App')
  .controller('LoginCtrl', ['$scope', 'auth', '$location', 'store','$route',function ($scope, auth, $location, store,$route) {

    $scope.login = function() {
      auth.signin({
      }, function(profile, idToken, accessToken, state, refreshToken) {
        store.set('profile', profile);
        store.set('token', idToken);
        $route.reload();
      }, function(err) {
        console.log("Error :(", err);
      });
    };

    $scope.logout = function() {
      auth.signout();
      store.remove('profile');
      store.remove('token');
      $route.reload();
    };

  }]);


