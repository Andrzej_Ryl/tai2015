'use strict';

/**
 * @ngdoc function
 * @name twojkandydat2015App.controller:CandidatesCtrl
 * @description
 * # CandidatesCtrl
 * Controller of the twojkandydat2015App
 */
angular.module('twojkandydat2015App')
  .controller('CandidatesCtrl',['$scope','$http',function ($scope,$http) {
    //$scope.candidates = [
    //  {
    //    Name: 'Bronisław',
    //    Surname: 'Komorowski',
    //    Image: 'http://cdn.nowiny.pl/im/v1/news-300x300-crop/2013/09/13/93649.jpg',
    //    Color: '#ED7600',
    //    Id: 0
    //  }
    //];

    // Using list before merge with backend

    // Konwertujemy kolor w hex na rgba

    $scope.convert = function convertHex(hex,opacity){
      hex = hex.replace('#','');
      var r = parseInt(hex.substring(0,2), 16);
      var g = parseInt(hex.substring(2,4), 16);
      var b = parseInt(hex.substring(4,6), 16);

      return 'rgba('+r+','+g+','+b+','+opacity/100+')';
    };

    // Centrujmey naszego grida

    var w = $(window);
    var centerList = function() {
      var width = $(window).width();
      width -= 350;
      var listsize = Math.floor(width / 300);
      if (listsize === 0) {
        listsize = 1;
      } else if (listsize > 4) {
        listsize = 4;
      }

      $scope.listsize = 300 * listsize;
      $('#candidates-list').css('max-width',$scope.listsize+'px');
    };



    w.bind('resize', centerList);

    $http.get('http://eryzim.nazwa.pl/twojkandydat/getCandidates.php')
      .success(function(response){
        console.log(response.records);
        $scope.candidates = response.records;
        $scope.candidates.forEach(function(entry){
          entry.Color = $scope.convert(entry.Color,60);
        });
        centerList();
      })
      .error(function(response){
        console.log(response);
      });





  }]);
