'use strict';

/**
 * @ngdoc function
 * @name twojkandydat2015App.controller:TimelineCtrl
 * @description
 * # TimelineCtrl
 * Controller of the twojkandydat2015App
 */
angular.module('twojkandydat2015App')
  .controller('TimelineCtrl', ['$scope','$http',function ($scope,$http) {

    //$scope.events = [
    //    {
    //      "id": "1",
    //      "TVname": "d",
    //      "description": "Wiec wyborczy Bronisława Komorowskiego",
    //      "host": "d",
    //      "candidate_id": "1",
    //      "location": 'Plac Centralny, Warszawa',
    //      "time": "2015-07-03T19:49:47Z",
    //      "type": "event"
    //    },
    //    {
    //      "id": "1",
    //      "content": "RT @onetpl: Oświadczenie @onetpl w sprawie spreparowanego materiału na temat @andrzejduda kroczącego w mediach społecznościowych: http://t.c...",
    //      "candidate_id": "1",
    //      "user_login": "AndrzejDuda",
    //      "user_name": "Andrzej Duda",
    //      "time": "2015-07-02T19:49:47Z",
    //      "type": "tweet"
    //    }
    //  ];

    // events from backend after merge

    $http.get('http://eryzim.nazwa.pl/twojkandydat/getTimeline.php')
      .success(function(response){
        console.log(response.records);
        $scope.events = response.records;
        checkForTweets($scope.events);
      })
      .error(function(response){
        console.log(response);
      });

    function sortByDate(array) {
      return array.sort(function(a, b) {
        var x = a.time; var y = b.time;
        return ((x < y) ? 1 : ((x > y) ? -1 : 0));
      });
    }

    var checkForTweets = function(events){
      events.forEach(function(entry){
        entry.tweet = entry.type === "tweet";
        entry.time = new Date(entry.time);
        if(entry.content && entry.content.substring(0,2) === "RT"){
          entry.content = "♻"+entry.content.slice(2,-1);
        }
      });
      sortByDate(events);
      console.log(events);
    };





  }]);
